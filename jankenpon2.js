var	
	piedra= document.getElementById("0"),
	papel = document.getElementById("1"),
	tijera = document.getElementById("2"),
	// variables del contador: [0] = Ganaste, [1] = empate, [2] = perdiste.
	maquinaCounter = [0,0,0],
	jugadorCounter = [0,0,0],
	contM = document.getElementById("contadorMaquina"),
	contJ = document.getElementById("contadorJugador"),
	resFin = document.getElementById("Final");
	//estas funciones imprimen los nodos de texto con los valores en el html.
	innerContJ = function () {
		contJ.innerHTML = "Has ganado: " + jugadorCounter[0] + " veces." + "</br>" + "Has empatado: " + jugadorCounter[1]  +" veces." + "</br>" + "Has perdido: " + jugadorCounter[2] + " veces.";
	};
	innerContM = function () {
		contM.innerHTML = "Has ganado: " + maquinaCounter[0] + " veces." + "</br>" + "Has empatado: " + maquinaCounter[1]  +" veces." + "</br>" + "Has perdido: " + maquinaCounter[2] + " veces.";
	}
	//estas lineas inician el contador en 0 cuando se carga la pagina.
	innerContJ()
	innerContM()
	//esta funcion te dice cuando acaba el juego.
function resultadoJuego () {

			if (jugadorCounter[0] == 5) {
				alert("¡FELICIDADES HAS GANADO!");
			}
			else if (maquinaCounter[0] == 5) {
				alert("¡LASTIMA HAS PERDIDO!");
			};
		}
function resetCounter () {
				if (jugadorCounter[0] == 5 || maquinaCounter[0] == 5 ) {
					maquinaCounter = [0,0,0];
					jugadorCounter = [0,0,0];
				};
}

//evento onclick de piedra.
piedra.addEventListener("click", function bla(){
	resetCounter()
	maquina = aleatorio(0,2);
	piedras = piedra.id;
	//esto regresa el resultado a blanco cada que reinicia el juego.
	document.opcionjugador.src = "img/cb.jpg" ;
	document.opcionmaquina.src = "img/cb.jpg" ;
	resFin.innerHTML= " ";
	//estas son funciones para darles el time out, solo para que no pase todo al mismo tiempo.
	function resJug () {document.opcionjugador.src = piedra.src;}
	setTimeout(resJug, 150)
	function resMaq () {document.opcionmaquina.src = document.getElementById(maquina).src;}
	setTimeout(resMaq, 500);
	function compro () {comprobacion(piedras,maquina);}
	setTimeout(compro,750)
	//estas lineas le dan update al contador
	setTimeout(innerContJ,1000);
	setTimeout(innerContM,1000);
	setTimeout(resultadoJuego,1100)
} ) 
//evento onclick de papel.
papel.addEventListener("click", function bla(){
	resetCounter()
	maquina = aleatorio(0,2);
	papeles = papel.id;
	//esto regresa el resultado a blanco cada que reinicia el juego.
	document.opcionjugador.src = "img/cb.jpg" ;
	document.opcionmaquina.src = "img/cb.jpg" ;
	resFin.innerHTML = " ";
	//estas son funciones para darles el time out, solo para que no pase todo al mismo tiempo.
	function resJug () {document.opcionjugador.src = papel.src;}
	setTimeout(resJug, 150)
	function resMaq () {document.opcionmaquina.src = document.getElementById(maquina).src;}
	setTimeout(resMaq, 500);
	function compro () {comprobacion(papeles,maquina);}
	setTimeout(compro,750)
	//estas lineas le dan update al contador
	setTimeout(innerContJ,1000);
	setTimeout(innerContM,1000);
	setTimeout(resultadoJuego,1100)

} ) 
//evento on click de tijera.
tijera.addEventListener("click", function bla(){
	resetCounter()
	maquina = aleatorio(0,2);
	tijeras = tijera.id;
	//esto regresa el resultado a blanco cada que reinicia el juego.
	document.opcionjugador.src = "img/cb.jpg" ;
	document.opcionmaquina.src = "img/cb.jpg" ;
	resFin.innerHTML = " ";

	//estas son funciones para darles el time out, solo para que no pase todo al mismo tiempo.    
	function resJug () {document.opcionjugador.src = tijera.src;}
	setTimeout(resJug, 150)
	function resMaq () {document.opcionmaquina.src = document.getElementById(maquina).src;}
	setTimeout(resMaq, 500);
	function compro () {comprobacion(tijeras,maquina);}
	setTimeout(compro,750)
	//estas lineas le dan update al contador
	setTimeout(innerContJ,1000);
	setTimeout(innerContM,1000);
	setTimeout(resultadoJuego,1100)
} ) 

//funcion para obtener un numero aleatorio entre sus parametros.
function aleatorio (minimo, maximo) {
		var numero = Math.floor( Math.random() * (maximo - minimo + 1) + minimo );
		return numero;
	};
//funcion para comprobar el resultado del juego "ganaste", "empataste", "perdiste".
function comprobacion (par1, par2) {
				//si es piedra corre este codigo.
				if (par1 == par2) {
					dime("Empate")
					resFin.style.color = "blue";
					jugadorCounter[1] += 1;
					maquinaCounter[1] += 1;
					
				};
				if (par1 == 0) {
						
						if (par2 == 1) {
							dime("Perdiste");
							resFin.style.color = "red";
							jugadorCounter[2] += 1;
							maquinaCounter[0] += 1;
							
						}
						else if (par2 == 2) {
							dime("Ganaste");
							resFin.style.color = "green";
							jugadorCounter[0] += 1;
							maquinaCounter[2] += 1;
							
						};
				}
				//si es papel corre este codigo.
				else if (par1 == 1) {
						if(par2 == 0){
							dime("Ganaste");
							resFin.style.color = "green";
							jugadorCounter[0] += 1;
							maquinaCounter[2] += 1;
							
						}
						else if (par2 == 2) {
							dime("Perdiste");
							resFin.style.color = "red";
							jugadorCounter[2] += 1;
							maquinaCounter[0] += 1;
							
						};
				}
				//si es tijera corre este codigo
				else if (par1 == 2) {
						if(par2 == 0){
							dime("Perdiste");
							resFin.style.color = "red";
							jugadorCounter[2] += 1;
							maquinaCounter[0] += 1;
							
						}
						else if (par2 == 1) {
							dime("Ganaste");
							resFin.style.color = "green";
							jugadorCounter[0] += 1;
							maquinaCounter[2] += 1;
							
						}
						
				};
};
function dime (arg1) {
	
	
	resFin.innerHTML = arg1;
	
}




